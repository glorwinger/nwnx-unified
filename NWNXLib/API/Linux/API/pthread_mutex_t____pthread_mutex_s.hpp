#pragma once

#include <cstdint>

#include "unknown_pthread_mutex_t____pthread_mutex_s__TLS_E82C02EE545DA7AED7F7BF450076CFFC.hpp"

namespace NWNXLib {

namespace API {

struct pthread_mutex_t____pthread_mutex_s
{
    int32_t __lock;
    uint32_t __count;
    int32_t __owner;
    int32_t __kind;
    uint32_t __nusers;
    pthread_mutex_t____pthread_mutex_s__TLS_E82C02EE545DA7AED7F7BF450076CFFC _anon_0;
};

}

}
