#pragma once

#include <cstdint>

#include "unknown_Expansion4compressionArray__TLS_78825147AF43802A65194B92D573C998.hpp"
#include "unknown_Expansion4orCompression.hpp"

namespace NWNXLib {

namespace API {

struct Expansion4compressionArray
{
    Expansion4orCompression type;
    Expansion4compressionArray__TLS_78825147AF43802A65194B92D573C998 expComp;
};

}

}
